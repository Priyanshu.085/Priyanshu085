# Welcome to Priyanshu's GitHub Profile! 👨‍💻

![Profile Banner](/Banner.png)


### Table Of Content
- [ABOUT ME](#about-me)
- [MY PROJECTS](#my-projects)
- [LANGUAGE AND TOOLS](#language-and-tools)
- [SOCIAL LINKS](#lets-connect)
- [LICENSE](#license)

## About Me
Hello! I'm Priyanshu, a passionate Full Stack Developer with a keen interest in Machine Learning. I specialize in Back-end Web Development and enjoy working on projects related to Generative AI and Prompt Engineering.

- 🌱 I’m currently learning more about Machine Learning and exploring new technologies.
- 💬 Ask me about Back-end Development, Generative AI, or anything related to Full Stack Development.
- 📫 How to reach me: abpriyanshu007@gmail.com.
- 😄 Pronouns: He/Him

[![An image of @priyanshu085's Holopin badges, which is a link to view their full Holopin profile](https://holopin.me/priyanshu085)](https://holopin.io/@priyanshu085)

## My Projects

### [1. NexDrive](https://github.com/Priyanshu085/NexDrive)
NexDrive is a dynamic car marketplace built with Next.js, Tailwind CSS, and TypeScript.
![NexDrive Screenshot](/screenshots/NexDrive.png)

### [2. PromptHub](https://github.com/Priyanshu085/Prompthub)
PromptHub is a prompt-sharing platform crafted with Next.js, Tailwind CSS, and MongoDB. Empowering creativity, it enables users to seamlessly share and explore prompts for writing and ideation.
![PromptHub Screenshot](/screenshots/Prompthub.png)

### [3. Portfolio](https://github.com/Priyanshu085/Portfolio)
A showcase of my personal projects and achievements.
![Portfolio Screenshot](/screenshots/Portfolio.png)

## Language and Tools
<p align="left"> 
<a href="https://www.w3schools.com/cpp/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/cplusplus/cplusplus-original.svg" alt="cplusplus" width="40" height="40"/> </a>
<a href="https://www.w3schools.com/css/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/> </a> 
<a href="https://cloud.google.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/google_cloud/google_cloud-icon.svg" alt="gcp" width="40" height="40"/> </a> 
<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="40" height="40"/> </a> 
<a href="https://reactjs.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original-wordmark.svg" alt="react" width="40" height="40"/> </a> 
<a href="https://www.typescriptlang.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/typescript/typescript-original.svg" alt="typescript" width="40" height="40"/> </a> 
</p>

## Let's Connect

<a href="https://linkedin.com/in/priyanshu085" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="priyanshu085" height="30" width="40" /></a> 
<a href="https://www.leetcode.com/priyanshu085" target="blank"><img align="center" src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/leet-code.svg" alt="priyanshu085" height="30" width="40" /></a>
</p>

<!-- 
- [Leetcode](https://leetcode.com/Priyanshu085/)
- [LinkedIn](https://www.linkedin.com/in/Priyanshu085/)
- [Google Developer Console](https://g.dev/priyanshu085) -->

## Fun Fact
I'm not funny 😉

## License
[MIT LICENSE](LICENSE)

<br>

<img align="left" src="https://github-readme-stats.vercel.app/api/top-langs?username=priyanshu085&show_icons=true&locale=en&layout=compact" alt="priyanshu085" width="400" height="250" />

&nbsp;<img align="center" src="https://github-readme-stats.vercel.app/api?username=priyanshu085&show_icons=true&locale=en" alt="priyanshu085" width="400" height="250" />

<img align="center" src="https://github-readme-streak-stats.herokuapp.com/?user=priyanshu085&" width="1000"  alt="priyanshu085" />

---

Thanks for visiting my profile! Feel free to explore my projects and don't hesitate to get in touch. 😊

